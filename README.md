# Python-PWr

# Prezentacje

* [22/04/2017 - Day 1](https://drive.google.com/open?id=0BxcsLaaVrpXOVGg3ZXItZTZocm8)
* [29/04/2017 - Day 2](https://drive.google.com/open?id=0BxcsLaaVrpXOOEFTcmVWZThrVTQ)

# Jupyter Notebook
* [29/04/2017 - Day 2](Michał_Janik/Python/Day2.ipynb.txt)
* [06/05/2017 - Day 3](Michał_Janik/Python/Day3.ipynb.txt)

# Code
* [27/05/2017 - Day 4](Michał_Janik/Python_Flask/app.py)

# Flask
* [Marcin Jenczmyk @ClearCode](Michał_Janik/Python_Flask/flask-presentation.pdf)